'use strict';

describe('ImpedimentListController', function(){
    var $controllerConstructor, mockImpedimentsApi;

    beforeEach(module("ImpedimentsApp"));

    beforeEach(inject(function($controller){
        mockImpedimentsApi = sinon.stub({getAllImpediments: function(){}, setCurrentImpedimentId: function(){}});
        $controllerConstructor = $controller;
    }));

    it('should set impediments to the result of the api get impediments', function(){
        var mockImpediments = {};
        mockImpedimentsApi.getAllImpediments.returns(mockImpediments);

        var ctrl = $controllerConstructor('ImpedimentListCtrl', {ImpedimentsApi: mockImpedimentsApi});
        expect(ctrl.impediments).toBe(mockImpediments);
    })


    it('should set the current impediment id to the selected impediment id', function(){
        var selectedId = '23';
        var ctrl = $controllerConstructor('ImpedimentListCtrl', {ImpedimentsApi: mockImpedimentsApi});

        ctrl.setCurrentImpedimentId(selectedId);

        expect(mockImpedimentsApi.setCurrentImpedimentId.calledWith('23')).toBe(true);
    })
})