'use strict'

describe('ImpedimentsDetailController', function(){
    var $controllerConstructor, mockImpedimentsApi;

    beforeEach(module("ImpedimentsApp"));

    beforeEach(inject(function($controller){
        mockImpedimentsApi = sinon.stub({updateImpediment: function(){}, getImpedimentData: function(){}, setCurrentImpedimentId: function(){}});
        $controllerConstructor = $controller;
    }));

    it('should get impediment data from api', function(){
        var mockImpediment = {summary: 'test'};
        mockImpedimentsApi.getImpedimentData.returns(mockImpediment);
        var ctrl =  $controllerConstructor('ImpedimentsDetailCtrl', {$stateParams: {riskId: '2'},ImpedimentsApi: mockImpedimentsApi});

        expect(ctrl.impediment).toBe(mockImpediment);
        expect(mockImpedimentsApi.getImpedimentData.calledWith('2'));
    })

    it('should expand or collapse impediment updates', function(){
        var ctrl =  $controllerConstructor('ImpedimentsDetailCtrl', {$stateParams: {},ImpedimentsApi: mockImpedimentsApi});

        expect(ctrl.isExpanded).toBe(false);

        ctrl.toggleUpdates();

        expect(ctrl.isExpanded).toBe(true);

        ctrl.toggleUpdates();

        expect(ctrl.isExpanded).toBe(false);
    })

    it('should not update an impediment with an empty comment', function(){
        var mockImpediment = {summary: 'test', updates:[]};
        mockImpedimentsApi.getImpedimentData.returns(mockImpediment);
        var ctrl =  $controllerConstructor('ImpedimentsDetailCtrl', {$stateParams: {},ImpedimentsApi: mockImpedimentsApi});

        expect(ctrl.impediment.updates.length).toBe(0);
        ctrl.newUpdate.comment = '';
        ctrl.updateImpediment();
        expect(ctrl.impediment.updates.length).toBe(0);
    })

    it('should not update an impediment with a null comment', function(){
        var mockImpediment = {summary: 'test', updates:[]};
        mockImpedimentsApi.getImpedimentData.returns(mockImpediment);
        var ctrl =  $controllerConstructor('ImpedimentsDetailCtrl', {$stateParams: {},ImpedimentsApi: mockImpedimentsApi});

        expect(ctrl.impediment.updates.length).toBe(0);
        ctrl.newUpdate.comment = null;

        ctrl.updateImpediment();

        expect(ctrl.impediment.updates.length).toBe(0);
    })


    it('should update an impediment with a non empty comment', function(){
        var mockImpediment = {summary: 'test', updates:[]};
        mockImpedimentsApi.getImpedimentData.returns(mockImpediment);

        var ctrl =  $controllerConstructor('ImpedimentsDetailCtrl', {$stateParams: {},ImpedimentsApi: mockImpedimentsApi});
        mockImpedimentsApi.updateImpediment.returns(ctrl.impediment);

        expect(ctrl.impediment.updates.length).toBe(0);
        ctrl.newUpdate.comment = 'text';

        ctrl.updateImpediment();

        expect(mockImpedimentsApi.updateImpediment.calledWith(ctrl.impediment));
        expect(ctrl.impediment.updates.length).toBe(1);
        expect(ctrl.newUpdate.comment).toBe('');
    })
})