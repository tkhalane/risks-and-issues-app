﻿(function(){
    'use strict';

    angular.module('ImpedimentsApp').controller('ImpedimentsDetailCtrl', ['$stateParams', 'ImpedimentsApi', ImpedimentsDetailCtrl]);

    function ImpedimentsDetailCtrl($stateParams, ImpedimentsApi) {
        var vm = this;
        vm.impediment = ImpedimentsApi.getImpedimentData($stateParams.riskId);
        vm.newUpdate = {};
        vm.isExpanded = false;

        vm.toggleUpdates = function () {
            vm.isExpanded = !vm.isExpanded;
        };

        vm.updateImpediment = function () {
            var _comment = vm.newUpdate.comment;
            if (!_comment || _comment === '') {
                return;
            }
            var Update = { date: new Date(), updatedBy: 'Tiisetso', comment: _comment };

            vm.impediment.updates.push(Update);
            vm.impediment = ImpedimentsApi.updateImpediment(vm.impediment);
            vm.newUpdate.comment = '';
        };

        vm.closeRisk = function () {
            vm.impediment.open = false;
        };
    }

})();
