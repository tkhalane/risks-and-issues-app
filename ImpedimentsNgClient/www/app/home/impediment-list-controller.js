﻿(function() {
    'use strict';

    angular.module('ImpedimentsApp').controller('ImpedimentListCtrl', ['ImpedimentsApi', ImpedimentListCtrl]);

    function ImpedimentListCtrl(ImpedimentsApi) {
        var vm = this;
        
        vm.sortOrder = {};

        vm.impediments = ImpedimentsApi.getAllImpediments();

        vm.setCurrentImpedimentId = function (selectedId) {
            ImpedimentsApi.setCurrentImpedimentId(selectedId);
        };
    };

})();
