(function() {
    'use strict';

    angular.module('ImpedimentsApp').controller('HomeCtrl', ['$scope', '$ionicModal', '$ionicPopover', 'ImpedimentsApi', HomeCtrl]);


    function HomeCtrl($scope, $ionicModal, $ionicPopover, ImpedimentsApi) {
        var vm = this;
        
        $ionicModal.fromTemplateUrl('templates/modal.html', { scope: $scope })
                                .then(function(modal) {
                                    $scope.modal = modal;
                                });

        $ionicPopover.fromTemplateUrl('templates/popover.html', { scope: $scope})
                                .then(function(popover) {
                                    $scope.popover = popover;
                                });

        vm.createImpediment = function(newImpediment) {
            ImpedimentsApi.createImpediment(newImpediment);
            $scope.modal.hide();
        };
    }

})();

