// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'ImpedimentsApp' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'ImpedimentsApp.services' is found in services.js
// 'ImpedimentsApp.controllers' is found in controllers.js
var impedimentsApiUrl = 'http://localhost:3000/api';
angular.module('ImpedimentsApp', ['ionic','ngResource'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
  // setup an abstract state for the tabs directive
   .state('home', {
    url: "/home",
    abstract: true,
    templateUrl: "templates/tabs.html"
   })
    
  .state('home.impediments', {
      url: '/impediments',
      views: {
        'impediments': {
            templateUrl: 'app/home/impediments.html'
        }
      }
    })
    .state('home.impediment-detail', {
      url: '/impediments/:riskId',
      views: {
        'impediments': {
          templateUrl: 'app/details/impediment-detail.html'
        }
      }
    });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/home/impediments');

});
