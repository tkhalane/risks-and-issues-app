(function() {
    'use strict';

    angular.module('ImpedimentsApp').factory('ImpedimentsApi', ['$resource', ImpedimentsApi]);

    function ImpedimentsApi($resource) {
        var server = $resource(impedimentsApiUrl + '/impediments/:id', { id: '@id' }, { 'update': { method: 'PUT' } });
        var impediments = server.query();

        function getImpedimentData(id) {
            return server.get({ id: id });
        }

        function getAllImpediments() {
            return impediments;
        }

        function updateImpediment(updatedImpediment) {
            return server.update(updatedImpediment);
        }

        function createImpediment(impediment) {
            if (!impediment || impediment.length < 1) return;

            var newImpediment = new server({
                summary: impediment.summary,
                description: impediment.description,
                severity: impediment.severity,
                open: true,
                proximity: impediment.proximity,
                updates: []
            });

            newImpediment.$save(function () {
                impediments.push(newImpediment);
            });
        }

        var setCurrentImpedimentId = function(selectedId){
            self.currentImpedimentId = selectedId;
        }

        var getCurrentImpedimentId = function(){
            return self.currentImpedimentId;
        }

        return {
            getAllImpediments: getAllImpediments,
            getImpedimentData: getImpedimentData,
            updateImpediment: updateImpediment,
            createImpediment: createImpediment,
            setCurrentImpedimentId: setCurrentImpedimentId
        };
    };

})();