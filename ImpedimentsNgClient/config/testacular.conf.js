module.exports = function(config){
    config.set({
        basePath: '../www',

        files: [
            '../test/lib/jquery-2.1.3.min.js',
            'lib/ionic/js/angular/angular.js',
            'lib/ionic/js/angular/angular-*.js',
            'lib/ionic/js/ionic.js',
            'lib/ionic/js/*.js',
            '../test/lib/angular-mocks.js',
            '../test/lib/sinon-1.14.0.js',
            'app/app.js',
            'app/details/impediment-details-controller.js',
            'app/**/*.js',
            '../test/unit/*.js'
        ],
        frameworks: ['jasmine'],
        autoWatch: true,
        browsers: ['Chrome'],
        plugins: [
            'karma-chrome-launcher',
            'karma-script-launcher',
            'karma-jasmine'
        ],
        junitReporter:{
            outputFile: 'test_out/unit.xml',
            suite: 'unit'
         }
     });

}


