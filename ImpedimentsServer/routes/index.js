var express = require('express');
var router = express.Router();

var risks = require('./impediments.js');

/*
 * Routes that can be accessed only by autheticated users
 */
router.get('/api/impediments', risks.getAllImpediments);
router.get('/api/impediments/:id', risks.getImpediment);
router.post('/api/impediments/', risks.create);
router.put('/api/impediments/', risks.update);
router.delete('/api/impediments/:id', risks.delete);

module.exports = router;
