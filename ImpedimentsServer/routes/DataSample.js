
data = [
    {
    status: 'open',
    loggedOn: Date.now(),    
    summary: 'Limited time to fulfill all requirements',
    severity: 'Medium',
    proximity: Date.now(),
    owner: 'beri',
    owner_id: ObjectId('54e4a3319b97c22d22bc2124'),
    project_id: ObjectId('54e4a3319b97c22d22bc2124'),
    description: 'There team is under immense pressure to deliver more with limited time and resources. This is already affecting morale',
    type:'issue',
    updates: [
         {   comment: 'We are waiting on bsg to provide way forward',
             date: Date.now(),
             updatedBy: 'Hugo'
         },
         {  comment: 'There has been no progress on this issue.',
            date: Date.now(),
            updatedBy: 'Coco'
        }]
    },
    {
    status: 'open',
    loggedOn: Date.now(),    
    summary: 'ECM storage space has been limited',
    severity: 'High',
    proximity: Date.now(),
    owner: 'tiisetso',
    owner_id: ObjectId('54e4a3319b97c22d22bc2124'),
    project_id: ObjectId('54e4a3319b97c22d22bc2124'),
    description: 'ECM Service has been one of the problematic services. Space has been limited and documents have been failing to upload successfully.',
    type:'risk',
    updates: [{  comment: 'Contact with management was made. No resolution yet',
             date: Date.now(),
             updatedBy: 'Tiisetso'
         },
         {   comment: 'Contact with client resources was made. No resolution yet',
             date: Date.now(),
             updatedBy: 'Hugo'
         },
         {   comment: 'The one developer on ECM has just left the project',
             date: Date.now(),
             updatedBy: 'Hugo'
         },
         {   comment: 'ECM department recruiting support developers to assist',
             date: Date.now(),
             updatedBy: 'Hugo'
         },
         {  comment: 'ECM have updated their infrastructure, this is resulting in massive service downtime',
            date: Date.now(),
            updatedBy: 'Coco'
        }]
    },
    {
    status: 'open',
    loggedOn: Date.now(),    
    summary: 'Lack of clarity and leadership',
    severity: 'High',
    proximity: Date.now(),
    owner: 'beri',
    owner_id: ObjectId('54e4a3319b97c22d22bc2124'),
    project_id: ObjectId('54e4a3319b97c22d22bc2124'),
    description: 'There is been a lot of tensions linked to political agendas within the team, this is affecting productivity.',
    type:'coco',
    updates: [{  comment: 'Contact with management was made. No resolution yet',
             date: Date.now(),
             updatedBy: 'Tiisetso'
         },
         {   comment: 'We are waiting on bsg to provide way forward',
             date: Date.now(),
             updatedBy: 'Hugo'
         },
         {  comment: 'There has been no progress on this issue.',
            date: Date.now(),
            updatedBy: 'Coco'
        }]
    },
    {
    status: 'open',
    loggedOn: Date.now(),    
    summary: 'WCF services returning bad data',
    severity: 'High',
    proximity: Date.now(),
    owner: 'sibusiso',
    owner_id: ObjectId('54e4a3319b97c22d22bc2124'),
    project_id: ObjectId('54e4a3319b97c22d22bc2124'),
    description: 'WCF Services have been returnign bad data.Space has been limited and documents have been failing to upload successfully.',
    type:'risk',
    updates: [{  comment: 'Contact with management was made. No resolution yet',
             date: Date.now(),
             updatedBy: 'Tiisetso'
         },
         {   comment: 'Contact with client resources was made. No resolution yet',
             date: Date.now(),
             updatedBy: 'Hugo'
         },
         {   comment: 'The one developer on ECM has just left the project',
             date: Date.now(),
             updatedBy: 'Hugo'
         },
         {   comment: 'ECM department recruiting support developers to assist',
             date: Date.now(),
             updatedBy: 'Hugo'
         },
         {  comment: 'ECM have updated their infrastructure, this is resulting in massive service downtime',
            date: Date.now(),
            updatedBy: 'Coco'
        }]
    },
    {
    status: 'open',
    loggedOn: Date.now(),    
    summary: 'Exergy data service failing with CIF errors',
    severity: 'High',
    proximity: Date.now(),
    owner: 'beri',
    owner_id: ObjectId('54e4a3319b97c22d22bc2124'),
    project_id: ObjectId('54e4a3319b97c22d22bc2124'),
    description: 'Source of funds source of income updates have resulted in many validation errors. Most services in exergy are failing as a result',
    type:'risk',
    updates: [{  comment: 'Contact with management was made. No resolution yet',
             date: Date.now(),
             updatedBy: 'Tiisetso'
         },
         {   comment: 'We are waiting on bsg to provide way forward',
             date: Date.now(),
             updatedBy: 'Hugo'
         },
         {  comment: 'There has been no progress on this issue.',
            date: Date.now(),
            updatedBy: 'Coco'
        }]
    },
    {
    status: 'open',
    loggedOn: Date.now(),    
    summary: 'ALVS Management facing difficulties',
    severity: 'High',
    proximity: Date.now(),
    owner: 'coco',
    owner_id: ObjectId('54e4a3319b97c22d22bc2124'),
    project_id: ObjectId('54e4a3319b97c22d22bc2124'),
    description: 'ALVS Service has been one of the problematic services. Space has been limited and documents have been failing to upload successfully.',
    type:'risk',
    updates: [
         {   comment: 'Contact with client resources was made. No resolution yet',
             date: Date.now(),
             updatedBy: 'Hugo'
         },
         {   comment: 'The one developer on ECM has just left the project',
             date: Date.now(),
             updatedBy: 'Hugo'
         },
         {   comment: 'ECM department recruiting support developers to assist',
             date: Date.now(),
             updatedBy: 'Hugo'
         },
         {  comment: 'ECM have updated their infrastructure, this is resulting in massive service downtime',
            date: Date.now(),
            updatedBy: 'Coco'
        }]
    }
    
    ];
    db.impediments.insert(data);