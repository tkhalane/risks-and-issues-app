﻿var mongoose = require('mongoose');

var ImpedimentSchema = new mongoose.Schema({
    status: String,
    loggedOn: Date,    
    summary: String,
    severity: String,
    proximity: Date,
    owner: String,
    //owner_id: mongoose.Types.ObjectId,
    //project_id: mongoose.Types.ObjectId,
    description: String,
    type: String,
    updates: Array
});

module.exports = mongoose.model('Impediment', ImpedimentSchema);