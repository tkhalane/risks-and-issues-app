var Impediment = require('./Impediment.js');
var _ = require('lodash');

var data = [];

var impediments = {
	getAllImpediments: function(req, res) {
	    Impediment.find(function(err, impediments) {
	        if (err){
	        	console.log('Error occurred: ', err);
	        	return;
	        } 
	        data = impediments;
	        res.json(impediments);
	    });
	},

    getImpediment: function(req, res) {
		var id = req.params.id;
		var impediment = _.find(data, function(impediment) {
			 return impediment._id == id;
			});
		res.json(impediment);
	},

	create: function(req, res) {
	    var newImpediment = req.body;
	    if(newImpediment){
	    	newImpediment.id = data.length;
		    newImpediment.owner = 'Tiisetso',// use logged in user
		    newImpediment.loggedOn = new Date();

		    Impediment.create(newImpediment, function (err, impediment) {
		    	if(err)	{
		    		console.log('error: ', err);
		    		return next(err);
		    	}
		    	data.push(impediment);
		    	res.json(impediment);
		    });
	    }
    },

	update: function (req, res) {
	    var newImpediment = req.body;
	   	Impediment.findByIdAndUpdate(newImpediment._id, newImpediment, function(err, impediment) {
	   		if (err) return next(err);
	   		console.log(impediment.updates);
	   		res.json(impediment);// also need to refresh the in memory Data collection
	   	});
	   
  	},

  	close: function (req, res) {
	   	Impediment.findByIdAndUpdate(newImpediment._id, { open: false}, function(err, impediment) {
	   		if (err) return next(err);
	   		console.log(impediment.updates); //also need to refresh the in memory Data collection
	   		res.json(impediment);
	   	});
	   
  	},


  	delete: function(req, res) {
	    var id = req.params.id;
  	    data.splice(id, 1); // Spoof a DB call
	    res.json(true);
  	}

};

module.exports = impediments;
